import React from 'react';
import './App.css';
import axios from 'axios';


function App() {
  
  const [decodedBase64, setBase64] = React.useState(null);
  const [data_json, setJson] = React.useState({});

  const OnChange = (evt) => {
    var file = evt.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = function (evt) {
      var base64 = reader.result;
      
      axios.post('https://rbaze8nq3m.execute-api.us-east-1.amazonaws.com/v1/analyze', {
        data: base64
      }).then(res => {
        setBase64(base64);
        if(res.data.statusCode && res.data.statusCode == 200)
          setJson(res.data.info);
        else
          setJson(res.data);
        console.log("res.status", res.status)
      }).catch(err => {
        console.log("errr", err.response);
      });
    }
  }

  return (
    <div className="App">
      
      <div className="box">
        <div>
          <img src={decodedBase64} width={400} height={400} alt="base 64" />
        </div>
        <div>
          <input type="file" onChange={OnChange} />
        </div>

        <div>
        {JSON.stringify(data_json)}
      </div> 
    </div>
  </div>
  );
}

export default App;
